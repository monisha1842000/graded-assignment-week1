package com.pack1;

public class SuperDep {

		public String depName() {
		return "SUPERDEPARTMENT ";
		}

		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
		return "NO WORK AS OF NOW ";
		}

		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
		return " NIL ";
		}

		//declare method isTodayAHoliday of type string
		public String isTodayAHoliday() {
		return "TODAY IS NOT HOLIDAY";
		}

}
